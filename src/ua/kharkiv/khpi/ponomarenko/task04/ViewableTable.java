package ua.kharkiv.khpi.ponomarenko.task04;

import ua.kharkiv.khpi.ponomarenko.task03.View;
import ua.kharkiv.khpi.ponomarenko.task03.ViewableResult;

// TODO: Auto-generated Javadoc
/**
 * The Class ViewableTable.
 */
public class ViewableTable extends ViewableResult {
	
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task03.ViewableResult#getView()
	 */
	@Override
	public View getView() {
		return new ViewTable();
	}
}
