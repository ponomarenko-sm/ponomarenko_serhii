package ua.kharkiv.khpi.ponomarenko.task06;

import ua.kharkiv.khpi.ponomarenko.task05.Command;

// TODO: Auto-generated Javadoc
/**
 * The Interface Queue.
 */
public interface Queue {
	
	/**
	 * Put.
	 *
	 * @param cmd the cmd
	 */
	void put(Command cmd);
	
	/**
	 * Take.
	 *
	 * @return the command
	 */
	Command take();
}
