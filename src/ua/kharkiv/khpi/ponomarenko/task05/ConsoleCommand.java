package ua.kharkiv.khpi.ponomarenko.task05;

// TODO: Auto-generated Javadoc
/**
 * The Interface ConsoleCommand.
 */
public interface ConsoleCommand extends Command {
	
	/**
	 * Gets the key.
	 *
	 * @return the key
	 */
	public char getKey();
}