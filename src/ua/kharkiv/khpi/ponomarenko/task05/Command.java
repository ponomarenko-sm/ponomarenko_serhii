package ua.kharkiv.khpi.ponomarenko.task05;

// TODO: Auto-generated Javadoc
/**
 * The Interface Command.
 */
public interface Command {
	
	/**
	 * Execute.
	 */
	public void execute();
}
