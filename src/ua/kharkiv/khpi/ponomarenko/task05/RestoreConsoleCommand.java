package ua.kharkiv.khpi.ponomarenko.task05;

import ua.kharkiv.khpi.ponomarenko.task03.View;

// TODO: Auto-generated Javadoc
/**
 * The Class RestoreConsoleCommand.
 */
public class RestoreConsoleCommand implements ConsoleCommand {
	
	/** The view. */
	private View view;
	
	/**
	 *  Инициализирует поле {@linkplain RestoreConsoleCommand#view}.
	 *
	 * @param view объект, реализующий интерфейс {@linkplain View}
	 */
	public RestoreConsoleCommand(View view) {
		this.view = view;
	}
	
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task05.ConsoleCommand#getKey()
	 */
	@Override
	public char getKey() {
		return 'r';
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "'r'estore";
	}
	
	/* (non-Javadoc)
	 * @see ua.kharkiv.khpi.ponomarenko.task05.Command#execute()
	 */
	@Override
	public void execute() {
		System.out.println("Restore last saved.");
		try {
			view.viewRestore();
		} catch (Exception e) {
			System.err.println("Serialization error: " + e);
		}
		view.viewShow();
	}
}
